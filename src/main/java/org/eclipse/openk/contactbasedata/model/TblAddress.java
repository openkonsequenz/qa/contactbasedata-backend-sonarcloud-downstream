/*
    *******************************************************************************
    * Copyright (c) 2019 Contributors to the Eclipse Foundation
    *
    * See the NOTICE file(s) distributed with this work for additional
    * information regarding copyright ownership.
    *
    * This program and the accompanying materials are made available under the
    * terms of the Eclipse Public License v. 2.0 which is available at
    * http://www.eclipse.org/legal/epl-2.0.
    *
    * SPDX-License-Identifier: EPL-2.0
    *******************************************************************************
*/
package org.eclipse.openk.contactbasedata.model;

import lombok.Data;

import jakarta.persistence.*;
import java.util.UUID;

@Data
@Entity
public class TblAddress {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE,  generator = "tbl_address_id_seq")
    @SequenceGenerator(name = "tbl_address_id_seq", sequenceName = "tbl_address_id_seq", allocationSize = 1)
    @Column(name = "id", updatable = false)
    private Long id;

    private UUID uuid;
    private Boolean isMainAddress;
    private String postcode;
    private String community;
    private String communitySuffix;
    private String street;
    private String housenumber;
    private String wgs_84_zone;
    private String latitude;
    private String longitude;
    private String urlMap;
    private String note;


    @ManyToOne
    @JoinColumn( name = "fk_contact_id")
    private TblContact tblContact;

    @OneToOne
    @JoinColumn( name = "fk_address_type")
    private RefAddressType refAddressType;

}
