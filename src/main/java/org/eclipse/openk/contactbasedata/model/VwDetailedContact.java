/*
 *******************************************************************************
 * Copyright (c) 2019 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
 */
package org.eclipse.openk.contactbasedata.model;

import lombok.Data;
import org.springframework.data.annotation.Immutable;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import java.util.UUID;

@Data
@Immutable
@Entity
public class VwDetailedContact {
    @Id
    private Long fkContactId;

    private Long id;
    private UUID uuid;
    private String name;
    private String contactType;
    private String companyName;
    private String companyType;
    private UUID companyId;
    private UUID salutationUuid;
    private UUID personTypeUuid;
    private String firstName;
    private String lastName;
    private String department;
    private String note;
    private String salutationType;
    private String personType;
    private String mainAddress;
    private String email;
    private String searchfield;
    private Boolean anonymized;

}
