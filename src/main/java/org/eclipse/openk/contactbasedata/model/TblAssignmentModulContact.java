/*
    *******************************************************************************
    * Copyright (c) 2019 Contributors to the Eclipse Foundation
    *
    * See the NOTICE file(s) distributed with this work for additional
    * information regarding copyright ownership.
    *
    * This program and the accompanying materials are made available under the
    * terms of the Eclipse Public License v. 2.0 which is available at
    * http://www.eclipse.org/legal/epl-2.0.
    *
    * SPDX-License-Identifier: EPL-2.0
    *******************************************************************************
*/
package org.eclipse.openk.contactbasedata.model;

import lombok.Data;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.SequenceGenerator;
import java.util.Date;
import java.util.UUID;

@Data
@Entity
public class TblAssignmentModulContact {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE,  generator = "TBL_ASSIGN_MODUL_CNTCT_ID_SEQ")
    @SequenceGenerator(name = "TBL_ASSIGN_MODUL_CNTCT_ID_SEQ", sequenceName = "TBL_ASSIGN_MODUL_CNTCT_ID_SEQ", allocationSize = 1)
    @Column(name = "id", updatable = false)
    private Long id;
    private UUID uuid;
    private String modulName;
    private Date assignmentDate;
    private Date expiringDate;
    private Date deletionLockUntil;
    private String assignmentNote;

    @ManyToOne
    @JoinColumn( name = "fk_contact_id")
    private TblContact tblContact;

}
