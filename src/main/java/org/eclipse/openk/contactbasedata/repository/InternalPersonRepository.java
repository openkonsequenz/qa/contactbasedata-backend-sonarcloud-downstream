/*
 *******************************************************************************
 * Copyright (c) 2019 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
 */
package org.eclipse.openk.contactbasedata.repository;

import org.eclipse.openk.contactbasedata.model.TblInternalPerson;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface InternalPersonRepository extends JpaRepository<TblInternalPerson, Long > {
    @Query("select ip from TblInternalPerson ip where ip.contact.uuid = ?1")
    Optional< TblInternalPerson > findByTblContactUuid(final UUID contactUuid);

    @Query( "select ip from TblInternalPerson ip where "+
            "(COALESCE(:uid) is null or COALESCE(:uid) is not null AND ip.uidIdent=:uid) AND " +
            "(COALESCE(:userRef) is null or COALESCE(:userRef) is not null AND ip.userRef=:userRef)")
    Page<TblInternalPerson> findByFilter(@Param( "uid") String uid,
                                         @Param( "userRef") String userRef,
                                         Pageable pageable);

    Page<TblInternalPerson> findByContact_anonymizedFalseOrContact_anonymizedIsNull(Pageable pageable); // NOSONAR fd 07.02.2020: Method name cannot be changed

    List<TblInternalPerson> findByUidIdentNotNull();

    List<TblInternalPerson> findByUserRefNotNull();
}
