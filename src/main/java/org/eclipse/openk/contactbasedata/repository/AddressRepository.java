/*
 *******************************************************************************
 * Copyright (c) 2019 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
 */
package org.eclipse.openk.contactbasedata.repository;

import org.eclipse.openk.contactbasedata.model.TblAddress;
import org.eclipse.openk.contactbasedata.model.TblContact;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface AddressRepository extends JpaRepository<TblAddress, Long> {

    public Optional<TblAddress> findByUuid(UUID uuid );

    @Query("select a from TblAddress a where a.tblContact.uuid = ?1")
    public List< TblAddress > findByTblContactUuid(final UUID contactUuid);

    Optional<TblAddress> findByTblContactAndUuid(TblContact tblContact, UUID uuid);

    @Query("select count(*) from TblAddress a where a.tblContact.id=:contactId and a.isMainAddress=:isMainAddress")
    Long countByContactIdAndIsMainAddress(@Param("contactId") Long contactId, @Param("isMainAddress") boolean isMainAddress);

    @Query("select count(*) from TblAddress a where a.tblContact.id=:contactId and a.isMainAddress=:isMainAddress and uuid <> :addressUuid")
    Long countByContactIdAndMainAddressIsNotSame(@Param("contactId") Long supplierId, @Param("isMainAddress") Boolean isMmainAddress, @Param("addressUuid") UUID addressUuid);
}


