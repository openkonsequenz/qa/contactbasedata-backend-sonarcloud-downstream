/*
 *******************************************************************************
 * Copyright (c) 2019 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
 */
package org.eclipse.openk.contactbasedata.repository;

import org.eclipse.openk.contactbasedata.model.TblContactPerson;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface ContactPersonRepository extends JpaRepository<TblContactPerson, Long > {

    @Query("select cp from TblContactPerson cp where cp.contact.uuid = ?1")
    Optional< TblContactPerson > findByTblContactUuid(final UUID contactUuid);

    List<TblContactPerson> findByContact_anonymizedFalseOrContact_anonymizedIsNull(); // NOSONAR fd 07.02.2020: Method name cannot be changed
}
