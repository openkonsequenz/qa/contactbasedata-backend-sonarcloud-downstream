/*
 *******************************************************************************
 * Copyright (c) 2019 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
 */
package org.eclipse.openk.contactbasedata.mapper;

import org.eclipse.openk.contactbasedata.model.TblContactPerson;
import org.eclipse.openk.contactbasedata.viewmodel.ContactPersonDto;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.ReportingPolicy;

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface ContactPersonMapper {

    @Mappings({
            @Mapping( source="contact.uuid", target="contactUuid"),
            @Mapping( source="contact.contactType", target="contactType"),
            @Mapping( source="contact.note", target="contactNote"),
            @Mapping( source="contact.anonymized", target="contactAnonymized"),
            @Mapping( source="refPersonType.uuid", target="personTypeUuid"),
            @Mapping( source="refPersonType.type", target="personType"),
            @Mapping( source="salutation.uuid", target="salutationUuid"),
            @Mapping( source="salutation.type", target="salutationType"),
            @Mapping( source="company.contact.uuid", target="companyContactUuid"),
            @Mapping( source="company.companyName", target="companyName"),
            @Mapping( source="company.companyType", target="companyType"),
            @Mapping( source="company.hrNumber", target="companyHrNumber"),

    })
    ContactPersonDto toContactPersonDto(TblContactPerson tblContactPerson);

    @Mappings({
            @Mapping( source="contactUuid", target="contact.uuid"),
            @Mapping( source="contactType", target="contact.contactType"),
            @Mapping( source="contactNote", target="contact.note"),
            @Mapping( source="personTypeUuid", target="refPersonType.uuid"),
            @Mapping( source="personType", target="refPersonType.type"),
            @Mapping( source="salutationUuid", target="salutation.uuid"),
            @Mapping( source="salutationType", target="salutation.type"),
            @Mapping( source="companyContactUuid", target="company.contact.uuid"),
            @Mapping( source="companyName", target="company.companyName"),
            @Mapping( source="companyType", target="company.companyType"),
            @Mapping( source="companyHrNumber", target="company.hrNumber"),
            @Mapping( source="contactAnonymized", target="contact.anonymized"),
    })
    TblContactPerson toTblContactPerson(ContactPersonDto contactPersonDto);
}
