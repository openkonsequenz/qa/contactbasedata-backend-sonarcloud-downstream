/*
 *******************************************************************************
 * Copyright (c) 2019 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
 */
package org.eclipse.openk.contactbasedata.mapper;

import org.eclipse.openk.contactbasedata.model.TblInternalPerson;
import org.eclipse.openk.contactbasedata.viewmodel.InternalPersonDto;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.ReportingPolicy;

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface InternalPersonMapper {


    @Mappings({
            @Mapping( source="contact.uuid", target="contactUuid"),
            @Mapping( source="contact.contactType", target="contactType"),
            @Mapping( source="contact.note", target="contactNote"),
            @Mapping( source="contact.anonymized", target="contactAnonymized"),
            @Mapping( source="refPersonType.uuid", target="personTypeUuid"),
            @Mapping( source="refPersonType.type", target="personType"),
            @Mapping( source="salutation.uuid", target="salutationUuid"),
            @Mapping( source="salutation.type", target="salutationType"),
            @Mapping( source="uidIdent", target="uid")
    })
    InternalPersonDto toInternalPersonDto(TblInternalPerson tblInternalPerson);

    @Mappings({
            @Mapping( source="contactUuid", target="contact.uuid"),
            @Mapping( source="contactType", target="contact.contactType"),
            @Mapping( source="contactNote", target="contact.note"),
            @Mapping( source="contactAnonymized", target="contact.anonymized"),
            @Mapping( source="personTypeUuid", target="refPersonType.uuid"),
            @Mapping( source="personType", target="refPersonType.type"),
            @Mapping( source="salutationUuid", target="salutation.uuid"),
            @Mapping( source="salutationType", target="salutation.type"),
            @Mapping( source="uid", target="uidIdent")
    })
    TblInternalPerson toTblInternalPerson(InternalPersonDto internalPersonDto);
}