/*
 *******************************************************************************
 * Copyright (c) 2019 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/

package org.eclipse.openk.contactbasedata.config.auth;

import com.auth0.jwt.JWT;
import com.auth0.jwt.interfaces.Claim;
import com.auth0.jwt.interfaces.DecodedJWT;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

@Component
public class JwtAuthenticationTokenFilter extends OncePerRequestFilter {

    /**
     * Prefix used in combination with the resource (client) name for resource level roles.
     */
    public static final String PREFIX_RESOURCE_ROLE = "ROLE_";

    /**
     * Name of the claim containing the realm level roles
     */
    private static final String CLAIM_REALM_ACCESS = "realm_access";
    /**
     * Name of the claim containing the resources (clients) the user has access to.
     */
    private static final String CLAIM_RESOURCE_ACCESS = "resource_access";

    private static final String CLAIM_ROLES = "roles";
    public static final String PREFERRED_USERNAME = "preferred_username";

    @Value("${jwt.useStaticJwt}")
    private boolean useStaticJwt;

    @Value("${jwt.tokenHeader}")
    private String tokenHeader;

    @Value("${jwt.staticJwt}")
    private String staticJwt;

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain) throws ServletException, IOException {
        String authenticationHeader = useStaticJwt ? staticJwt : request.getHeader(this.tokenHeader);
        try {
            SecurityContext context= SecurityContextHolder.getContext();

            if(authenticationHeader != null && !authenticationHeader.isEmpty()) {

                final String bearerTkn= authenticationHeader.replace("Bearer ", "");

                createToken(context, bearerTkn);

            }
            chain.doFilter(request, response);
        } catch(AuthenticationException ex) {
            throw new ServletException("Authentication exception.");
        }

    }

    private void createToken(SecurityContext context, String bearerTkn) throws ServletException {
        try {
            DecodedJWT decodedJWT = JWT.decode(bearerTkn);
            Collection<String> allRoles = extractRoles(decodedJWT);

            List<GrantedAuthority> authorities= new ArrayList<>();
            allRoles.forEach( x -> authorities.add(new SimpleGrantedAuthority(PREFIX_RESOURCE_ROLE + x.toUpperCase())));

            String preferredUsername = decodedJWT.getClaim(PREFERRED_USERNAME).asString();
            UsernamePasswordAuthenticationToken auth = new UsernamePasswordAuthenticationToken(preferredUsername, null, authorities);
            auth.setDetails(bearerTkn);

            context.setAuthentication(auth);

        } catch (Exception e) {
            throw new ServletException("Invalid token.");
        }
    }

    public static Collection<String> extractRoles(DecodedJWT decodedJWT) {
        Set<String> roles = new HashSet<>();

        // Extract client roles from realm_access
        Optional.ofNullable(decodedJWT.getClaim(CLAIM_REALM_ACCESS))
                .map(Claim::asMap)
                .map(realmRoles -> (Collection<String>) realmRoles.get(CLAIM_ROLES))
                .ifPresent(roles::addAll);

        // Extract client roles from resource_access
        Map<String, Object> resourceAccessMap = decodedJWT.getClaim(CLAIM_RESOURCE_ACCESS).asMap();
        if (resourceAccessMap != null) {
            for (Map.Entry<String, Object> client : resourceAccessMap.entrySet()) {
                Map<String, Object> value = (Map<String, Object>) client.getValue();
                List<String> clientRoles = (List<String>) value.getOrDefault(CLAIM_ROLES, new ArrayList<>());
                roles.addAll(clientRoles);
            }
        }
        return roles;
    }

}
