/*
 *******************************************************************************
 * Copyright (c) 2019 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
 */
package org.eclipse.openk.contactbasedata.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import lombok.extern.log4j.Log4j2;
import org.eclipse.openk.contactbasedata.exceptions.BadRequestException;
import org.eclipse.openk.contactbasedata.service.AssignmentModulContactService;
import org.eclipse.openk.contactbasedata.viewmodel.AssignmentModulContactDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.util.List;
import java.util.UUID;

@Log4j2
@RestController
@RequestMapping("/contacts")
public class AssignmentModulContactController {
    @Autowired
    private AssignmentModulContactService assignmentModulContactService;

    @GetMapping ("/{contactUuid}/assignments")
    @Secured({"ROLE_KON-READER", "ROLE_KON-WRITER", "ROLE_KON-ADMIN"})
    @Operation(summary = "Anzeigen aller Zuordnungen Kontakt:Modul")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "400", description = "Ungültige Anfrage."),
            @ApiResponse(responseCode = "200", description = "Suche durchgeführt")})
    @ResponseStatus(HttpStatus.OK)
    public List<AssignmentModulContactDto> getAssignment(
             @PathVariable UUID contactUuid) {
        return assignmentModulContactService.getAssignments(contactUuid);
    }

    @GetMapping("/{contactUuid}/assignments/{assignmentUuid}")
    @Secured({"ROLE_KON-READER", "ROLE_KON-WRITER", "ROLE_KON-ADMIN"})
    @Operation(summary = "Lesen einer Zuordnung Kontakt:Modul")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "404", description = "Zuordnung nicht gefunden."),
            @ApiResponse(responseCode = "400", description = "Ungültige Anfrage."),
            @ApiResponse(responseCode = "200", description = "Zuordnung erfolgreich ermittelt.")})
    public AssignmentModulContactDto getAssignment(
            @PathVariable UUID contactUuid,
            @PathVariable UUID assignmentUuid) {
        return assignmentModulContactService.getAssignment(contactUuid, assignmentUuid);
    }


    @PostMapping("/{contactUuid}/assignments")
    @Secured({"ROLE_KON-WRITER", "ROLE_KON-ADMIN"})
    @Operation(summary = "Anlegen einer neuen Zuordnung Kontakt:Modul")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Zuordnung erfolgreich angelegt"),
            @ApiResponse(responseCode = "500", description = "Konnte nicht durchgeführt werden")
    })
    public ResponseEntity<AssignmentModulContactDto> insertAssignment(
            @PathVariable UUID contactUuid,
            @Validated @RequestBody AssignmentModulContactDto assignmentModulContactDto) {
        if (!contactUuid.equals(assignmentModulContactDto.getContactUuid())) {
            throw new BadRequestException("invalid.uuid.path.object");
        }
        AssignmentModulContactDto savedAssignment = assignmentModulContactService.insertAssignment(contactUuid, assignmentModulContactDto);
        URI location = ServletUriComponentsBuilder
                .fromCurrentRequestUri()
                .path("/{uuid}")
                .buildAndExpand(savedAssignment.getUuid())
                .toUri();
        return ResponseEntity.created(location).body(savedAssignment);
    }


    @PutMapping("/{contactUuid}/assignments/{assignmentUuid}")
    @Secured({"ROLE_KON-WRITER", "ROLE_KON-ADMIN"})
    @Operation(summary = "Ändern einer Zuordnung Kontakt:Modul")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "404", description = "Zuordnung nicht gefunden."),
            @ApiResponse(responseCode = "400", description = "Ungültige Anfrage."),
            @ApiResponse(responseCode = "200", description = "Zuordnung erfolgreich geändert.")})
    public ResponseEntity updateAssignment(
            @PathVariable UUID contactUuid,
            @PathVariable UUID assignmentUuid,
            @Validated @RequestBody AssignmentModulContactDto assignmentDto) {
        if (!assignmentUuid.equals(assignmentDto.getUuid())) {
            throw new BadRequestException("invalid.uuid.path.object");
        }
        assignmentModulContactService.updateAssignment(contactUuid, assignmentDto);
        return ResponseEntity.ok().build();
    }

    @DeleteMapping("{contactUuid}/assignments/{assignmentUuid}")
    @Secured({"ROLE_KON-WRITER", "ROLE_KON-ADMIN"})
    @ResponseStatus(HttpStatus.OK)
    @Operation(summary = "Eine bestimmte Zuordnung eines bestimmten Kontakts löschen")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "204", description = "Erfolgreich gelöscht"),
            @ApiResponse(responseCode = "400", description = "Ungültige Anfrage"),
            @ApiResponse(responseCode = "404", description = "Nicht gefunden")})
    public void deleteAssignment(@PathVariable("contactUuid") UUID contactUuid,
                              @PathVariable("assignmentUuid") UUID assignmentUuid) {
        assignmentModulContactService.deleteAssignment(contactUuid, assignmentUuid);
    }

}
