/*
 *******************************************************************************
 * Copyright (c) 2019 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
 */
package org.eclipse.openk.contactbasedata.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import lombok.extern.log4j.Log4j2;
import org.eclipse.openk.contactbasedata.model.KeyCloakUser;
import org.eclipse.openk.contactbasedata.service.UserService;
import org.eclipse.openk.contactbasedata.viewmodel.UserModule;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@Log4j2
@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    private UserService userService;

    @Operation(summary = "Ermitteln der UserModules vom Auth'n'Auth-Service")
    @ApiResponses(value = {@ApiResponse(responseCode = "200", description = "Erfolgreich durchgeführt")})
    @GetMapping("/modules")
    @Secured({"ROLE_KON-READER", "ROLE_KON-WRITER", "ROLE_KON-ADMIN"})
    public List<UserModule> getUserModules() {
        return userService.getUserModules();
    }

    @Operation(summary = "Ermitteln der KeycloakUsers vom Auth'n'Auth-Service")
    @ApiResponses(value = {@ApiResponse(responseCode = "200", description = "Erfolgreich durchgeführt")})
    @GetMapping("/keycloak-users")
    @Secured({"ROLE_KON-READER", "ROLE_KON-WRITER", "ROLE_KON-ADMIN"})
    public List<KeyCloakUser> getKeycloakUsers() {
        return userService.getKeycloakUsers();
    }



}
