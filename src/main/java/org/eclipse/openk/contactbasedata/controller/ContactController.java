/*
 *******************************************************************************
 * Copyright (c) 2019 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
 */
package org.eclipse.openk.contactbasedata.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import lombok.extern.log4j.Log4j2;
import org.eclipse.openk.contactbasedata.model.VwDetailedContact;
import org.eclipse.openk.contactbasedata.service.ContactAnonymizerService;
import org.eclipse.openk.contactbasedata.service.ContactService;
import org.eclipse.openk.contactbasedata.service.util.SearchContactsFilterParams;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.util.Optional;
import java.util.UUID;

import static org.springframework.data.domain.Sort.Direction.ASC;

@Log4j2
@RestController
@RequestMapping("/contacts")
public class ContactController {
    @Autowired
    private ContactService contactService;

    @Autowired
    private ContactAnonymizerService contactAnonymizerService;

    @Operation(summary = "Anzeigen aller gespeicherter Kontakte")
    @ApiResponses(value = {@ApiResponse(responseCode = "200", description = "Erfolgreich durchgeführt")})
    @ResponseStatus(HttpStatus.OK)
    @Secured({"ROLE_KON-READER", "ROLE_KON-WRITER", "ROLE_KON-ADMIN"})
    @GetMapping
    public Page<VwDetailedContact> findContacts(
            @RequestParam( "contactType") Optional<String> contactType,
            @RequestParam( "personTypeId" ) Optional<UUID> personTypeUuid,
            @RequestParam( "searchText" ) Optional<String> searchText,
            @RequestParam( "moduleName") Optional<String> moduleName,
            @RequestParam( "withoutAssignedModule") Optional<Boolean> withoutAssignedModule,
            @RequestParam( "expiringDataInPast" ) Optional<Boolean> expiringDataInPast,
            @RequestParam( "deletionLockExceeded") Optional<Boolean> delLockExceeded,
            @RequestParam( "alsoShowAnonymized" ) Optional<Boolean> showAnonymized,
            @RequestParam( "withSyncError") Optional<Boolean> showWithSyncError,
            @PageableDefault( sort = {"name"}, size = 20, direction = ASC) Pageable pageable ) {

        SearchContactsFilterParams filter = new SearchContactsFilterParams();
        filter.setContactType(contactType.orElse(null));
        filter.setPersonTypeUuid(personTypeUuid.orElse(null));
        filter.setSearchText(searchText.orElse(null));
        filter.setModuleName(moduleName.orElse(null));
        filter.setWithoutModule(withoutAssignedModule.orElse(Boolean.FALSE));
        filter.setExpiringDataInPast(expiringDataInPast.orElse(Boolean.FALSE));
        filter.setDelLockExceeded(delLockExceeded.orElse(Boolean.FALSE));
        filter.setShowAnonymized(showAnonymized.orElse(Boolean.FALSE));
        filter.setWithSyncError(showWithSyncError.orElse(Boolean.FALSE));

        return contactService.findDetailedContacts(
                filter, pageable);
    }


    @Operation(summary = "Anzeigen eines gespeicherten Kontaktes")
    @ApiResponses(value = {@ApiResponse(responseCode = "200", description = "Erfolgreich durchgeführt")})
    @ResponseStatus(HttpStatus.OK)
    @Secured({"ROLE_KON-READER", "ROLE_KON-WRITER", "ROLE_KON-ADMIN"})
    @GetMapping("/{contactUuid}")
    public VwDetailedContact findContact(
            @PathVariable( "contactUuid") UUID contactUuid){
            return contactService.findDetailedContactByUuid(contactUuid);
    }

    @Operation(summary = "Anonymisieren eines Kontaktes")
    @ApiResponses(value = {
        @ApiResponse(responseCode = "200", description = "Kontakt wurde anonymisiert"),
        @ApiResponse(responseCode = "404", description = "Nicht gefunden")})
    @ResponseStatus(HttpStatus.OK)
    @Secured({"ROLE_KON-WRITER", "ROLE_KON-ADMIN"})
    @PutMapping("/{contactUuid}/anonymize")
    public ResponseEntity anonymizeContact(
            @PathVariable("contactUuid") String contactUuid) {

        contactAnonymizerService.anonymize(UUID.fromString(contactUuid));
        return ResponseEntity.ok().build();
    }
}
