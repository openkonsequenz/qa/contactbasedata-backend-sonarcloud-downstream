/*
 *******************************************************************************
 * Copyright (c) 2019 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
 */
package org.eclipse.openk.contactbasedata.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import lombok.extern.log4j.Log4j2;
import org.eclipse.openk.contactbasedata.exceptions.BadRequestException;
import org.eclipse.openk.contactbasedata.exceptions.ConflictException;
import org.eclipse.openk.contactbasedata.service.CommunicationTypeService;
import org.eclipse.openk.contactbasedata.viewmodel.CommunicationTypeDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.util.List;
import java.util.UUID;

@Log4j2
@RestController
@RequestMapping("/communication-types")
public class CommunicationTypeController {
    @Autowired
    private CommunicationTypeService communicationTypeService;


    @GetMapping("/{communicationTypeUuid}")
    @Secured({"ROLE_KON-READER", "ROLE_KON-WRITER", "ROLE_KON-ADMIN"})
    @Operation(summary = "Anzeigen eines Kommunikationstyps")
    @ApiResponses(value = { @ApiResponse(responseCode = "200", description = "Erfolgreich durchgeführt"),
                            @ApiResponse(responseCode = "404", description = "Person wurde nicht gefunden")})
    @ResponseStatus(HttpStatus.OK)
    public CommunicationTypeDto getCommunicationType(@PathVariable UUID communicationTypeUuid) {
        return communicationTypeService.findCommunicationType(communicationTypeUuid);
    }

    @GetMapping
    @Secured({"ROLE_KON-READER", "ROLE_KON-WRITER", "ROLE_KON-ADMIN"})
    @Operation(summary = "Anzeigen aller Kommunikationstypen")
    @ResponseStatus(HttpStatus.OK)
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Erfolgreich durchgeführt")})
    public List<CommunicationTypeDto> getCommunicationTypes(){
            return communicationTypeService.findCommunicationTypes();
    }

    @PostMapping
    @Secured({"ROLE_KON-ADMIN"})
    @Operation(summary = "Anlegen eines Kommunikationstyps")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Kommunikationstyp erfolgreich angelegt"),
            @ApiResponse(responseCode = "500", description = "Konnte nicht durchgeführt werden")
    })
    public ResponseEntity<CommunicationTypeDto> insertCommunicationType(
            @Validated @RequestBody CommunicationTypeDto communicationTypeDto) {
        CommunicationTypeDto savedCommunicationTypeDto = communicationTypeService.insertCommunicationType(communicationTypeDto);
        URI location = ServletUriComponentsBuilder
                .fromCurrentRequestUri()
                .path("/{uuid}")
                .buildAndExpand(savedCommunicationTypeDto.getUuid())
                .toUri();
        return ResponseEntity.created(location).body(savedCommunicationTypeDto);
    }

    @PutMapping("/{uuid}")
    @Secured({"ROLE_KON-ADMIN"})
    @Operation(summary = "Ändern eines Kommunikationstyps")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Kommunikationstyp wurde aktualisiert"),
            @ApiResponse(responseCode = "400", description = "Ungültige Eingabe"),
            @ApiResponse(responseCode = "404", description = "Nicht gefunden")})
    public ResponseEntity updateCommunicationType(@PathVariable UUID uuid, @Validated @RequestBody CommunicationTypeDto communicationTypeDto) {

        if (!communicationTypeDto.getUuid().equals(uuid)) {
            throw new BadRequestException("invalid.uuid.path.object");
        }

        communicationTypeService.updateCommunicationType(communicationTypeDto);
        return ResponseEntity.ok().build();
    }

    @DeleteMapping("/{uuid}")
    @Secured("ROLE_KON-ADMIN")
    @ResponseStatus(HttpStatus.OK)
    @Operation(summary = "Kommunikationstyp löschen")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "204", description = "Erfolgreich durchgeführt"),
            @ApiResponse(responseCode = "400", description = "Ungültige Anfrage"),
            @ApiResponse(responseCode = "404", description = "Nicht gefunden"),
            @ApiResponse(responseCode = "409", description = "Datensatz konnte nicht gelöscht werden")})
    public void removeComunicationType(@PathVariable UUID uuid) {
        try {
            communicationTypeService.removeCommunicationType(uuid);
        }
        catch( Exception ex ) {
            throw new ConflictException("Communication type couldn't be deleted");
        }
    }
}

