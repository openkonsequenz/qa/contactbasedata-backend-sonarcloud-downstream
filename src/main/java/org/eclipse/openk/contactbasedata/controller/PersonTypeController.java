/*
 *******************************************************************************
 * Copyright (c) 2019 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
 */
package org.eclipse.openk.contactbasedata.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import lombok.extern.log4j.Log4j2;
import org.eclipse.openk.contactbasedata.exceptions.BadRequestException;
import org.eclipse.openk.contactbasedata.exceptions.ConflictException;
import org.eclipse.openk.contactbasedata.service.PersonTypeService;
import org.eclipse.openk.contactbasedata.viewmodel.PersonTypeDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.util.List;
import java.util.UUID;

@Log4j2
@RestController
@RequestMapping("/personTypes")
public class PersonTypeController {
    @Autowired
    private PersonTypeService personTypeService;

    @Operation(summary = "Anzeigen aller Personentypen")
    @Secured({"ROLE_KON-READER", "ROLE_KON-WRITER", "ROLE_KON-ADMIN"})
    @ApiResponses(value = { @ApiResponse(responseCode = "200", description = "Erfolgreich durchgeführt")})
    @ResponseStatus(HttpStatus.OK)
    @GetMapping
    public List<PersonTypeDto> getPersonType() {
        return personTypeService.findAllPersonTypes();
    }



    @GetMapping("/{personTypeUuid}")
    @Secured({"ROLE_KON-READER", "ROLE_KON-WRITER", "ROLE_KON-ADMIN"})
    @Operation(summary = "Suchen eines Personentyps per UUID")
    @ResponseStatus(HttpStatus.OK)
    @ApiResponses(value = {
            @ApiResponse(responseCode = "400", description = "Ungültige Anfrage."),
            @ApiResponse(responseCode = "200", description = "Personentyp gefunden")})
    public PersonTypeDto getPersonType(@PathVariable UUID personTypeUuid) {

        return personTypeService.getPersonTypeByUuid(personTypeUuid);
    }


    @PostMapping
    @Secured("ROLE_KON-ADMIN")
    @Operation(summary = "Anlegen eines neuen Personentyps")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Personentyp erfolgreich angelegt"),
            @ApiResponse(responseCode = "500", description = "Konnte nicht durchgeführt werden")
    })
    public ResponseEntity<PersonTypeDto> insertPersonType(@Validated @RequestBody PersonTypeDto personTypeDto) {
        PersonTypeDto savedPersonTypeDto = personTypeService.insertPersonType(personTypeDto);
        URI location = ServletUriComponentsBuilder
                .fromCurrentRequestUri()
                .path("/{uuid}")
                .buildAndExpand(savedPersonTypeDto.getUuid())
                .toUri();
        return ResponseEntity.created(location).body(savedPersonTypeDto);
    }


    @PutMapping("/{personTypeUuid}")
    @Secured("ROLE_KON-ADMIN")
    @Operation(summary = "Ändern eines Personentyp")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Personentyp wurde erfolgreich aktualisiert"),
            @ApiResponse(responseCode = "400", description = "Ungültige Eingabe"),
            @ApiResponse(responseCode = "404", description = "Nicht gefunden")})
    public ResponseEntity updatePersonType(@PathVariable UUID personTypeUuid, @Validated @RequestBody PersonTypeDto personTypeDto) {

        if (!personTypeDto.getUuid().equals(personTypeUuid)) {
            throw new BadRequestException("invalid.uuid.path.object");
        }

        personTypeService.updatePersonType(personTypeDto);
        return ResponseEntity.ok().build();
    }


    @DeleteMapping("/{uuid}")
    @Secured("ROLE_KON-ADMIN")
    @ResponseStatus(HttpStatus.OK)
    @Operation(summary = "Personentyp löschen")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "204", description = "Erfolgreich durchgeführt"),
            @ApiResponse(responseCode = "400", description = "Ungültige Anfrage"),
            @ApiResponse(responseCode = "404", description = "Nicht gefunden"),
            @ApiResponse(responseCode = "409", description = "Datensatz konnte nicht gelöscht werden")})
    public void removePersonType(@PathVariable UUID uuid) {

        try {
            personTypeService.removePersonType(uuid);
        }
        catch( Exception ex ) {
            throw new ConflictException("Person type couldn't be deleted");
        }
    }

}
