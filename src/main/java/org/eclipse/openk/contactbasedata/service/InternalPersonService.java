/*
 *******************************************************************************
 * Copyright (c) 2019 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
 */
package org.eclipse.openk.contactbasedata.service;

import lombok.extern.log4j.Log4j2;
import org.eclipse.openk.contactbasedata.constants.Constants;
import org.eclipse.openk.contactbasedata.exceptions.NotFoundException;
import org.eclipse.openk.contactbasedata.mapper.ContactMapper;
import org.eclipse.openk.contactbasedata.mapper.InternalPersonMapper;
import org.eclipse.openk.contactbasedata.model.TblContact;
import org.eclipse.openk.contactbasedata.model.TblInternalPerson;
import org.eclipse.openk.contactbasedata.repository.ContactRepository;
import org.eclipse.openk.contactbasedata.repository.InternalPersonRepository;
import org.eclipse.openk.contactbasedata.repository.PersonTypeRepository;
import org.eclipse.openk.contactbasedata.repository.SalutationRepository;
import org.eclipse.openk.contactbasedata.viewmodel.InternalPersonDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.UUID;

@Log4j2
@Service
public class InternalPersonService {
    @Autowired
    private InternalPersonRepository internalPersonRepository;

    @Autowired
    private ContactRepository contactRepository;

    @Autowired
    private SalutationRepository salutationRepository;

    @Autowired
    private PersonTypeRepository personTypeRepository;

    @Autowired
    private InternalPersonMapper internalPersonMapper;

    @Autowired
    private ContactMapper contactMapper;

    @Autowired
    private BaseContactService baseContactService;

    public InternalPersonDto findInternalPerson(UUID contactUuid) {
        return internalPersonMapper.toInternalPersonDto(
                internalPersonRepository.findByTblContactUuid(contactUuid).orElseThrow(NotFoundException::new)
        );
    }

    public Page<InternalPersonDto> findInternalPersons( boolean showAlsoAnonymous, String uid, String userRef, Pageable pageable) {
        Page<TblInternalPerson> returnPageDb;
        if( uid == null && userRef == null) {
            returnPageDb = showAlsoAnonymous ?
                      internalPersonRepository.findAll( pageable )
                    : internalPersonRepository.findByContact_anonymizedFalseOrContact_anonymizedIsNull(pageable);
        }
        else {
            returnPageDb =  internalPersonRepository.findByFilter( uid, userRef, pageable );
        }
        return returnPageDb.map(internalPersonMapper::toInternalPersonDto);
    }

    @Transactional
    public InternalPersonDto insertInternalPerson(InternalPersonDto internalPersonDto) {
        TblContact contactToSave = new TblContact();
        contactToSave.setUuid(UUID.randomUUID());
        contactToSave.setContactType(Constants.CONTACT_TYPE_INTERNAL_PERSON);

        TblInternalPerson internalPersonToSave = internalPersonMapper.toTblInternalPerson(internalPersonDto);
        internalPersonToSave.setContact(contactToSave);
        contactRepository.save(internalPersonToSave.getContact());

        setFromInternalPersonDto( internalPersonToSave, internalPersonDto );
        // Save Contact first

        // Then save dependent Model-Object
        return internalPersonMapper.toInternalPersonDto(internalPersonRepository.save(internalPersonToSave));
    }

    @Transactional
    public InternalPersonDto updateInternalPerson(InternalPersonDto internalPersonDto){
        TblInternalPerson internalPersonUpdated;

        //Interne Person holen
        TblInternalPerson existingInternalPerson = internalPersonRepository
                .findByTblContactUuid(internalPersonDto.getContactUuid())
                .orElseThrow(() -> new NotFoundException("contact.uuid.not.existing"));

        existingInternalPerson.setLastName(internalPersonDto.getLastName());
        existingInternalPerson.setFirstName(internalPersonDto.getFirstName());
        existingInternalPerson.setTitle(internalPersonDto.getTitle());
        existingInternalPerson.setDepartment(internalPersonDto.getDepartment());
        existingInternalPerson.setUidIdent(internalPersonDto.getUid());
        existingInternalPerson.setUserRef(internalPersonDto.getUserRef());
        existingInternalPerson.setSyncNote(internalPersonDto.getSyncNote());
        existingInternalPerson.setIsSyncError(internalPersonDto.getIsSyncError());

        setFromInternalPersonDto( existingInternalPerson, internalPersonDto );
        internalPersonUpdated = internalPersonRepository.save(existingInternalPerson);

        return internalPersonMapper.toInternalPersonDto(internalPersonUpdated);
    }

    private void setFromInternalPersonDto( TblInternalPerson destTblInternalPerson, InternalPersonDto sourceDto ) {

        if( sourceDto.getSalutationUuid() != null ) {
            destTblInternalPerson.setSalutation( salutationRepository
                    .findByUuid(sourceDto.getSalutationUuid())
                    .orElseThrow(() -> new NotFoundException("salutation.uuid.not.existing")));
        }
        else {
            destTblInternalPerson.setSalutation(null);
        }

        if( sourceDto.getPersonTypeUuid() != null ) {
            destTblInternalPerson.setRefPersonType( personTypeRepository
                    .findByUuid(sourceDto.getPersonTypeUuid())
                    .orElseThrow(() -> new NotFoundException("person.type.uuid.not.existing")));
        }
        else {
            destTblInternalPerson.setRefPersonType(null);
        }

        destTblInternalPerson.getContact().setNote(sourceDto.getContactNote());
        destTblInternalPerson.getContact().setAnonymized(sourceDto.getContactAnonymized());
    }
}
