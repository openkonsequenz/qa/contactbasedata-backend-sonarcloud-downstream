/*
 *******************************************************************************
 * Copyright (c) 2019 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
 */
package org.eclipse.openk.contactbasedata.viewmodel;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import jakarta.validation.constraints.NotBlank;
import java.io.Serializable;
import java.util.UUID;

@Data
public class ContactPersonDto implements Serializable {

    private String firstName;

    @Schema(requiredMode = Schema.RequiredMode.REQUIRED)
    @NotBlank(message = "Lastname is mandatory")
    private String lastName;

    private String title;

    //from Contact
    @JsonProperty("contactId")
    private UUID contactUuid;
    private String contactType;
    private String contactNote;
    private Boolean contactAnonymized;


    //from Company
    @JsonProperty("companyContactId")
    private UUID companyContactUuid;
    private String companyName;
    private String companyType;
    private String companyHrNumber;

    @JsonProperty("personTypeId")
    private UUID personTypeUuid;
    private String personType;

    //from RefSalutation
    @JsonProperty("salutationId")
    private UUID salutationUuid;
    private String salutationType;

    private String email;
}
