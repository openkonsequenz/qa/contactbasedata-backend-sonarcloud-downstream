/*
 *******************************************************************************
 * Copyright (c) 2019 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
 */
package org.eclipse.openk.contactbasedata.viewmodel;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import jakarta.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.UUID;

@Data
public class AddressDto implements Serializable {

    @JsonProperty("id")
    private UUID uuid;

    @JsonProperty("contactId")
    private UUID contactUuid;

    private Boolean isMainAddress;
    private String postcode;
    private String community;
    private String communitySuffix;
    private String street;
    private String housenumber;
    private String wgs84Zone;
    private String latitude;
    private String longitude;
    private String urlMap;
    private String note;

    //fromAddressType
    @JsonProperty("addressTypeId")
    @Schema(requiredMode = Schema.RequiredMode.REQUIRED)
    @NotNull(message = "Field 'adressTypeUuid' is mandatory")
    private UUID addressTypeUuid;

    private String addressTypeType;
    private String addressTypeDescription;
}
