/*
 *******************************************************************************
 * Copyright (c) 2019 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
 */
package org.eclipse.openk.contactbasedata.viewmodel;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import jakarta.validation.constraints.NotBlank;
import java.io.Serializable;
import java.util.UUID;

@Data
public class CommunicationTypeDto implements Serializable {

    @JsonIgnore
    private Long id;

    @JsonProperty("id")
    private UUID uuid;

    @Schema(requiredMode = Schema.RequiredMode.REQUIRED)
    @NotBlank(message = "Field 'type' is mandatory")
    private String  type;

    private String description;
    private boolean editable;
    private boolean mappingLdap;
    private boolean typeEmail;
}
