/*
 *******************************************************************************
 * Copyright (c) 2019 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
 */
package org.eclipse.openk.contactbasedata.config;

import org.eclipse.openk.contactbasedata.ContactBaseDataApplication;
import org.eclipse.openk.contactbasedata.api.AuthNAuthApi;
import org.eclipse.openk.contactbasedata.mapper.*;
import org.eclipse.openk.contactbasedata.service.*;
import org.eclipse.openk.contactbasedata.service.util.LdapUserAttributesMapper;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.ldap.core.LdapTemplate;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;

@EnableJpaRepositories(basePackages = "org.eclipse.openk.contactbasedata")
@EntityScan(basePackageClasses = ContactBaseDataApplication.class)
@TestPropertySource("spring.config.location=classpath:application.yml")
public class TestConfiguration {
    @Bean
    VersionMapper versionMapper() { return new VersionMapperImpl(); }

    @Bean
    VersionMapperImpl versionMapperImpl() { return new VersionMapperImpl(); }

    @Bean
    ExternalPersonMapper externalPersonMapper() { return new ExternalPersonMapperImpl(); }

    @Bean
    InternalPersonMapper internalPersonMapper() { return new InternalPersonMapperImpl(); }

    @Bean
    ContactPersonMapper contactPersonMapper() { return new ContactPersonMapperImpl(); }

    @Bean
    CompanyMapper companyMapper() { return new CompanyMapperImpl(); }

    @Bean
    ContactMapper contactMapper() { return new ContactMapperImpl(); }

    @Bean
    SalutationMapper salutationMapper() { return new SalutationMapperImpl(); }

    @Bean
    PersonTypeMapper personTypeMapper() { return new PersonTypeMapperImpl(); }

    @Bean
    AddressTypeMapper addressTypeMapper() { return new AddressTypeMapperImpl(); }

    @Bean
    AddressMapper addressMapper() { return new AddressMapperImpl(); }

    @Bean
    CommunicationTypeMapper communicationTypeMapper() { return new CommunicationTypeMapperImpl(); }

    @Bean
    CommunicationMapper communicationMapper() { return new CommunicationMapperImpl(); }

    @Bean
    AssignmentModulContactMapper assignmentModulContactMapper() { return new AssignmentModulContactMapperImpl(); }

    @Bean
    LdapUserAttributesMapper ldapUserAttributesMapper() { return new LdapUserAttributesMapper(); };

    @MockBean
    private AuthNAuthApi authNAuthApi;

    @MockBean
    private LdapTemplate ldapTemplate;

    @Bean
    public VersionService myVersionService() {
        return new VersionService();
    }

    @Bean
    public ContactService myContactService() { return new ContactService(); }

    @Bean
    public ExternalPersonService myExternalPersonService() { return new ExternalPersonService(); }

    @Bean
    public InternalPersonService myInternalPersonService() { return new InternalPersonService(); }

    @Bean
    public ContactPersonService myContactPersonService() { return new ContactPersonService(); }

    @Bean
    public CompanyService myCompanyService() { return new CompanyService(); }

    @Bean
    public SalutationService mySalutationService() { return new SalutationService(); }

    @Bean
    public PersonTypeService myPersonTypeService() { return new PersonTypeService(); }

    @Bean
    public AddressTypeService myAddressTypeService() { return new AddressTypeService(); }

    @Bean
    public AddressService myAddressService() { return new AddressService(); }

    @Bean
    public CommunicationTypeService myCommunicationTypeService() { return new CommunicationTypeService(); }

    @Bean
    public CommunicationService myCommunicationService() { return new CommunicationService(); }

    @Bean
    public BaseContactService myBaseContactService() { return new BaseContactService(); }

    @Bean
    public AssignmentModulContactService myAssignmentModulContactService() { return new AssignmentModulContactService(); }

    @Bean
    public ContactAnonymizerService myContactAnonymizerService() { return new ContactAnonymizerService(); }

    @Bean
    public LdapService myLdapService() { return new LdapService(); }

    @Bean
    public AuthNAuthService myAuthNAuthService() { return new AuthNAuthService(); }
}
