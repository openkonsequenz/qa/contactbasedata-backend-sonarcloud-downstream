/*
 *******************************************************************************
 * Copyright (c) 2019 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
 */
package org.eclipse.openk.contactbasedata.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.eclipse.openk.contactbasedata.ContactBaseDataApplication;
import org.eclipse.openk.contactbasedata.service.ExternalPersonService;
import org.eclipse.openk.contactbasedata.support.MockDataHelper;
import org.eclipse.openk.contactbasedata.viewmodel.ExternalPersonDto;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;

import java.util.UUID;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyBoolean;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@SpringBootTest(classes = ContactBaseDataApplication.class)
@AutoConfigureMockMvc
@ActiveProfiles("test")
class ExternalPersonControllerTest {

    @MockBean
    private ExternalPersonService externalPersonService;

    @Autowired
    private MockMvc mockMvc;


    @Test
    void shouldReturnARequestedExtPerson() throws Exception {
        ExternalPersonDto ep = MockDataHelper.mockExternalPersonDto();

        when(externalPersonService.findExternalPerson(any(UUID.class))).thenReturn(ep);

        mockMvc.perform(get("/external-persons/"+UUID.randomUUID().toString()))
                .andExpect(status().is2xxSuccessful())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("firstName", is(  ep.getFirstName())));
    }


    @Test
    void shouldReturnAllExtPerson() throws Exception {
        Page<ExternalPersonDto> epPage = MockDataHelper.mockExternalPersonDtoPage();

        when(externalPersonService.findExternalPersons(anyBoolean(), any(Pageable.class))).thenReturn(epPage);

        mockMvc.perform(get("/external-persons"))
                .andExpect(status().is2xxSuccessful())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("totalElements", is(  2 )));
    }

    @Test
    void shouldUpdateExtPerson() throws Exception {
        ExternalPersonDto ep = MockDataHelper.mockExternalPersonDto();

        when( externalPersonService.updateExternalPerson(any(ExternalPersonDto.class))).thenReturn(ep);

        mockMvc.perform(put("/external-persons/{uuid}", ep.getContactUuid().toString())
                .contentType(MediaType.APPLICATION_JSON)
                .content(new ObjectMapper().writeValueAsString(ep)))
                .andExpect(status().is2xxSuccessful());
    }

    @Test
    void shouldNotUpdateExtPersonDueToException() throws Exception {
        ExternalPersonDto ep = MockDataHelper.mockExternalPersonDto();

        when( externalPersonService.updateExternalPerson(any(ExternalPersonDto.class))).thenReturn(ep);

        // provide different exception in url and object
        mockMvc.perform(put("/external-persons/{uuid}", UUID.randomUUID().toString())
                .contentType(MediaType.APPLICATION_JSON)
                .content(new ObjectMapper().writeValueAsString(ep)))
                .andExpect(status().isBadRequest());
    }

    @Test
    void shouldInsertExtPerson() throws Exception {
        ExternalPersonDto ep = MockDataHelper.mockExternalPersonDto();

        when( externalPersonService.insertExternalPerson(any(ExternalPersonDto.class))).thenReturn(ep);

        mockMvc.perform(post("/external-persons")
                .contentType(MediaType.APPLICATION_JSON)
                .content(new ObjectMapper().writeValueAsString(ep)))
                .andExpect(jsonPath("$.lastName", is(ep.getLastName())))
                .andExpect(jsonPath("$.contactId", not(ep.getContactUuid())))
                .andExpect(jsonPath("$.contactNote", is(ep.getContactNote())))
                .andExpect(status().is2xxSuccessful());
    }

}