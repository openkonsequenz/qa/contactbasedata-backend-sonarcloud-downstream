/*
 *******************************************************************************
 * Copyright (c) 2019 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
 */
package org.eclipse.openk.contactbasedata.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.eclipse.openk.contactbasedata.ContactBaseDataApplication;
import org.eclipse.openk.contactbasedata.service.CompanyService;
import org.eclipse.openk.contactbasedata.support.MockDataHelper;
import org.eclipse.openk.contactbasedata.viewmodel.CompanyDto;
import org.eclipse.openk.contactbasedata.viewmodel.ContactPersonDto;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;

import java.util.List;
import java.util.UUID;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyBoolean;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@SpringBootTest(classes = ContactBaseDataApplication.class)
@AutoConfigureMockMvc
@ActiveProfiles("test")
class CompanyControllerTest {

    @MockBean
    private CompanyService companyService;

    @Autowired
    private MockMvc mockMvc;


    @Test
    void shouldReturnARequestedCompany() throws Exception {
        CompanyDto ep = MockDataHelper.mockCompanyDto();

        when(companyService.findCompany(any(UUID.class))).thenReturn(ep);

        mockMvc.perform(get("/companies/"+UUID.randomUUID().toString()))
                .andExpect(status().is2xxSuccessful())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("companyName", is(  ep.getCompanyName())));
    }


    @Test
    void shouldReturnAllCompany() throws Exception {
        Page<CompanyDto> epPage = MockDataHelper.mockCompanyDtoPage();

        when(companyService.findCompanies(anyBoolean(), any(Pageable.class))).thenReturn(epPage);

        mockMvc.perform(get("/companies"))
                .andExpect(status().is2xxSuccessful())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("totalElements", is(  2 )));
    }

    @Test
    void shouldUpdateCompany() throws Exception {
        CompanyDto ep = MockDataHelper.mockCompanyDto();

        when( companyService.updateCompany(any(CompanyDto.class))).thenReturn(ep);

        mockMvc.perform(put("/companies/{uuid}", ep.getContactUuid().toString())
                .contentType(MediaType.APPLICATION_JSON)
                .content(new ObjectMapper().writeValueAsString(ep)))
                .andExpect(status().is2xxSuccessful());
    }

    @Test
    void shouldNotUpdateCompanyDueToException() throws Exception {
        CompanyDto ep = MockDataHelper.mockCompanyDto();

        when( companyService.updateCompany(any(CompanyDto.class))).thenReturn(ep);

        // provide different exception in url and object
        mockMvc.perform(put("/companies/{uuid}", UUID.randomUUID().toString())
                .contentType(MediaType.APPLICATION_JSON)
                .content(new ObjectMapper().writeValueAsString(ep)))
                .andExpect(status().isBadRequest());
    }

    @Test
    void shouldInsertCompany() throws Exception {
        CompanyDto companyDto = MockDataHelper.mockCompanyDto();

        when( companyService.insertCompany(any(CompanyDto.class))).thenReturn(companyDto);

        mockMvc.perform(post("/companies")
                .contentType(MediaType.APPLICATION_JSON)
                .content(new ObjectMapper().writeValueAsString(companyDto)))
                .andExpect(jsonPath("$.companyName", is(companyDto.getCompanyName())))
                .andExpect(jsonPath("$.contactId", not(companyDto.getContactUuid())))
                .andExpect(jsonPath("$.contactNote", is(companyDto.getContactNote())))
                .andExpect(status().is2xxSuccessful());
    }

    @Test
    void shouldReturnContactPerson() throws Exception {
        List<ContactPersonDto> contactPersonDtos = MockDataHelper.mockContactPersonDtos();

        when( companyService.findContactPersonsToCompany(any(UUID.class), anyBoolean())).thenReturn(contactPersonDtos);

        mockMvc.perform(get("/companies/"+UUID.randomUUID().toString()+"/contact-persons" ))
                .andExpect(status().is2xxSuccessful())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON));
    }
}