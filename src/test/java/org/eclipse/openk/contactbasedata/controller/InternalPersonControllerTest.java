/*
 *******************************************************************************
 * Copyright (c) 2019 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
 */
package org.eclipse.openk.contactbasedata.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.eclipse.openk.contactbasedata.ContactBaseDataApplication;
import org.eclipse.openk.contactbasedata.service.InternalPersonService;
import org.eclipse.openk.contactbasedata.support.MockDataHelper;
import org.eclipse.openk.contactbasedata.viewmodel.InternalPersonDto;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;

import java.util.UUID;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyBoolean;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@SpringBootTest(classes = ContactBaseDataApplication.class)
@AutoConfigureMockMvc
@ActiveProfiles("test")
class InternalPersonControllerTest {

    @MockBean
    private InternalPersonService internalPersonService;

    @Autowired
    private MockMvc mockMvc;


    @Test
    void shouldReturnARequestedIntPerson() throws Exception {
        InternalPersonDto ip = MockDataHelper.mockInternalPersonDto();

        when(internalPersonService.findInternalPerson(any(UUID.class))).thenReturn(ip);

        mockMvc.perform(get("/internal-persons/"+UUID.randomUUID().toString()))
                .andExpect(status().is2xxSuccessful())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("firstName", is(  ip.getFirstName())));
    }


    @Test
    void shouldReturnAllIntPersons() throws Exception {
        Page<InternalPersonDto> ipPage = MockDataHelper.mockInternalPersonDtoPage();

        when(internalPersonService.findInternalPersons(anyBoolean(), anyString(), anyString(), any(Pageable.class))).thenReturn(ipPage);

        mockMvc.perform(get("/internal-persons?uid=ABC&userRef=123"))
                .andExpect(status().is2xxSuccessful())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("totalElements", is(  2 )));
    }

    @Test
    void shouldUpdateIntPerson() throws Exception {
        InternalPersonDto ip = MockDataHelper.mockInternalPersonDto();

        when( internalPersonService.updateInternalPerson(any(InternalPersonDto.class))).thenReturn(ip);

        mockMvc.perform(put("/internal-persons/{uuid}", ip.getContactUuid().toString())
                .contentType(MediaType.APPLICATION_JSON)
                .content(new ObjectMapper().writeValueAsString(ip)))
                .andExpect(status().is2xxSuccessful());
    }

    @Test
    void shouldNotUpdateIntPersonDueToException() throws Exception {
        InternalPersonDto ip = MockDataHelper.mockInternalPersonDto();

        when( internalPersonService.updateInternalPerson(any(InternalPersonDto.class))).thenReturn(ip);

        // provide different exception in url and object
        mockMvc.perform(put("/internal-persons/{uuid}", UUID.randomUUID().toString())
                .contentType(MediaType.APPLICATION_JSON)
                .content(new ObjectMapper().writeValueAsString(ip)))
                .andExpect(status().isBadRequest());
    }

    @Test
    void shouldInsertIntPerson() throws Exception {
        InternalPersonDto ip = MockDataHelper.mockInternalPersonDto();

        when( internalPersonService.insertInternalPerson(any(InternalPersonDto.class))).thenReturn(ip);

        mockMvc.perform(post("/internal-persons")
                .contentType(MediaType.APPLICATION_JSON)
                .content(new ObjectMapper().writeValueAsString(ip)))
                .andExpect(jsonPath("$.lastName", is(ip.getLastName())))
                .andExpect(jsonPath("$.contactId", not(ip.getContactUuid())))
                .andExpect(jsonPath("$.contactNote", is(ip.getContactNote())))
                .andExpect(status().is2xxSuccessful());
    }

}