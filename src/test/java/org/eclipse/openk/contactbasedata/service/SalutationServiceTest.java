/*
 *******************************************************************************
 * Copyright (c) 2019 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
 */
package org.eclipse.openk.contactbasedata.service;

import org.eclipse.openk.contactbasedata.config.TestConfiguration;
import org.eclipse.openk.contactbasedata.model.RefSalutation;
import org.eclipse.openk.contactbasedata.repository.SalutationRepository;
import org.eclipse.openk.contactbasedata.support.MockDataHelper;
import org.eclipse.openk.contactbasedata.viewmodel.SalutationDto;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static org.hibernate.validator.internal.util.Contracts.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.isA;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.when;

@DataJpaTest
@ContextConfiguration(classes = {TestConfiguration.class})
@ActiveProfiles("test")
class SalutationServiceTest {
    @Qualifier("mySalutationService")
    @Autowired
    private SalutationService salutationService;

    @MockBean
    private SalutationRepository salutationRepository;

    @Test
    void shouldFindAllSalutationsProperly() {
        List<RefSalutation> rs = MockDataHelper.mockRefSalutations();
        when(salutationRepository.findAll()).thenReturn(rs);
        List<SalutationDto> retVals = salutationService.findAllSalutations();

        assertEquals(rs.size(), retVals.size());
        assertEquals(2, rs.size());
        assertEquals(rs.get(1).getUuid(), retVals.get(1).getUuid());
    }


    @Test
    void shouldReturnSalutationByUuid() {

        RefSalutation refSalutation = MockDataHelper.mockRefSalutation();
        refSalutation.setType("Testtyp");
        when( salutationRepository.findByUuid( any(UUID.class)) ).thenReturn(Optional.of(refSalutation));

        SalutationDto salutationDto = salutationService.getSalutationByUuid(UUID.randomUUID());
        assertEquals( salutationDto.getUuid(), refSalutation.getUuid() );
        assertEquals( salutationDto.getType(), refSalutation.getType() );
    }

    @Test
    void shouldInsertSalutation(){
        RefSalutation refSalutation = MockDataHelper.mockRefSalutation();

        refSalutation.setUuid(UUID.fromString("1468275e-200b-11ea-978f-2e728ce88125"));
        refSalutation.setType("Moin Herr");
        refSalutation.setDescription("Anrede nordisch männlich");

        SalutationDto salutationDto = MockDataHelper.mockSalutationDto();
        salutationDto.setUuid(null);

        when( salutationRepository.save( any( RefSalutation.class) )).thenReturn(refSalutation);

        SalutationDto savedSalutationDto = salutationService.insertSalutation(salutationDto);
        assertEquals("Moin Herr", savedSalutationDto.getType());
        assertNotNull( savedSalutationDto.getUuid());
    }

    @Test
    void shouldUpdateSalutation() {

        RefSalutation salutation = MockDataHelper.mockRefSalutation();
        salutation.setType("Moin Frau");

        SalutationDto salutationDto = MockDataHelper.mockSalutationDto();
        salutationDto.setType("Moin Herr");

        when( salutationRepository.findByUuid( any(UUID.class)) ).thenReturn(Optional.of(salutation));
        when( salutationRepository.save( any(RefSalutation.class)) ).thenReturn(salutation);
        SalutationDto salutationDtoUpdated = salutationService.updateSalutation(salutationDto);

        assertEquals(salutation.getUuid(), salutationDtoUpdated.getUuid());
        assertEquals("Moin Frau", salutationDtoUpdated.getType());
    }


    @Test
    void shouldDeleteSalutation() {

        RefSalutation salutation = MockDataHelper.mockRefSalutation();

        when(salutationRepository.findByUuid(any(UUID.class))).thenReturn(Optional.of(salutation));
        Mockito.doNothing().when(salutationRepository).delete( isA( RefSalutation.class ));
        salutationService.removeSalutation(salutation.getUuid());

        Mockito.verify(salutationRepository, times(1)).delete( salutation );
    }
}
