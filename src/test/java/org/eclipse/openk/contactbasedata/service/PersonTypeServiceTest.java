/*
 *******************************************************************************
 * Copyright (c) 2019 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
 */
package org.eclipse.openk.contactbasedata.service;

import org.eclipse.openk.contactbasedata.config.TestConfiguration;
import org.eclipse.openk.contactbasedata.model.RefPersonType;
import org.eclipse.openk.contactbasedata.repository.PersonTypeRepository;
import org.eclipse.openk.contactbasedata.support.MockDataHelper;
import org.eclipse.openk.contactbasedata.viewmodel.PersonTypeDto;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static org.hibernate.validator.internal.util.Contracts.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.isA;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.when;

@DataJpaTest
@ContextConfiguration(classes = {TestConfiguration.class})
@ActiveProfiles("test")
class PersonTypeServiceTest {
    @Qualifier("myPersonTypeService")
    @Autowired
    private PersonTypeService personTypeService;

    @MockBean
    private PersonTypeRepository personTypeRepository;

    @Test
    void shouldFindAllPersonTypesProperly() {
        List<RefPersonType> rs = MockDataHelper.mockRefPersonTypes();
        when(personTypeRepository.findAll()).thenReturn(rs);
        List<PersonTypeDto> retVals = personTypeService.findAllPersonTypes();

        assertEquals(rs.size(), retVals.size());
        assertEquals(2, rs.size());
        assertEquals(rs.get(1).getUuid(), retVals.get(1).getUuid());
    }


    @Test
    void shouldReturnPersonTypeByUuid() {

        RefPersonType refPersonType = MockDataHelper.mockRefPersonType();
        refPersonType.setType("Testtyp");
        when( personTypeRepository.findByUuid( any(UUID.class)) ).thenReturn(Optional.of(refPersonType));

        PersonTypeDto personTypeDto = personTypeService.getPersonTypeByUuid(UUID.randomUUID());
        assertEquals( personTypeDto.getUuid(), refPersonType.getUuid() );
        assertEquals( personTypeDto.getType(), refPersonType.getType() );
    }

    @Test
    void shouldInsertPersonType(){
        RefPersonType refPersonType = MockDataHelper.mockRefPersonType();

        refPersonType.setUuid(UUID.fromString("1468275e-200b-11ea-978f-2e728ce88125"));
        refPersonType.setType("Kein guter Mensch");
        refPersonType.setDescription("heutiger Standard Personentyp");

        PersonTypeDto personTypeDto = MockDataHelper.mockPersonTypeDto();
        personTypeDto.setUuid(null);

        when( personTypeRepository.save( any( RefPersonType.class) )).thenReturn(refPersonType);

        PersonTypeDto savedPersonTypeDto = personTypeService.insertPersonType(personTypeDto);
        assertEquals("Kein guter Mensch", savedPersonTypeDto.getType());
        assertNotNull( savedPersonTypeDto.getUuid());
    }

    @Test
    void shouldUpdatePersonType() {

        RefPersonType personType = MockDataHelper.mockRefPersonType();
        personType.setType("Kein schöner Mensch");

        PersonTypeDto personTypeDto = MockDataHelper.mockPersonTypeDto();
        personTypeDto.setType("Kein guter Mensch");

        when( personTypeRepository.findByUuid( any(UUID.class)) ).thenReturn(Optional.of(personType));
        when( personTypeRepository.save( any(RefPersonType.class)) ).thenReturn(personType);
        PersonTypeDto personTypeDtoUpdated = personTypeService.updatePersonType(personTypeDto);

        assertEquals(personType.getUuid(), personTypeDtoUpdated.getUuid());
        assertEquals("Kein schöner Mensch", personTypeDtoUpdated.getType());
    }


    @Test
    void shouldDeletePersonType() {

        RefPersonType personType = MockDataHelper.mockRefPersonType();

        when(personTypeRepository.findByUuid(any(UUID.class))).thenReturn(Optional.of(personType));
        Mockito.doNothing().when(personTypeRepository).delete( isA( RefPersonType.class ));
        personTypeService.removePersonType(personType.getUuid());

        Mockito.verify(personTypeRepository, times(1)).delete( personType );
    }
}
